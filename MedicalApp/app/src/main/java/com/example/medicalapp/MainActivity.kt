package com.example.medicalapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.medicalapp.databinding.ActivityMainBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var binding: ActivityMainBinding
    private val fileResult = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrar)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        auth = Firebase.auth
        binding.user.setOnClickListener {
            val intent = Intent(this, UserActivity::class.java)
            this.startActivity(intent)
        }
        binding.doctor.setOnClickListener {
            val intent = Intent(this, CItaActivity::class.java)
            this.startActivity(intent)
        }
        binding.medicament.setOnClickListener {
            val intent = Intent(this, MedicamentActivity::class.java)
            this.startActivity(intent)
        }
        binding.signOutImageView.setOnClickListener {
            Firebase.auth.signOut()
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

    }


}
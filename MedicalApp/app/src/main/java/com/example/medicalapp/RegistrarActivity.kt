package com.example.medicalapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.medicalapp.databinding.ActivityRegistrarBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.ktx.Firebase
import java.util.regex.Pattern


class RegistrarActivity : AppCompatActivity() {
    private lateinit var database: DatabaseReference
    private lateinit var binding: ActivityRegistrarBinding
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrar)
        binding = ActivityRegistrarBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        auth = Firebase.auth

        binding.signOutImageView.setOnClickListener {
            Firebase.auth.signOut()
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)}

        binding.signUpButton.setOnClickListener {
            val mEmail = binding.emailEditText.text.toString()
            val mPassword = binding.passwordEditText.text.toString()
            val mRepeatPassword = binding.repeatPasswordEditText.text.toString()
            val userId=mEmail
            database.child("users").child(userId).child("username").setValue(userId)
            val name = "nombre"
            database.child("users").child(userId).child("username").setValue(name)
            val direccion = "direccion"
            database.child("users")
            database.child("users").child(userId).child("username").setValue(direccion)
            val telefono="telefono"
            database.child("users").child(userId).child("username").setValue(telefono)
            val edad="edad"
            database.child("users").child(userId).child("username").setValue(edad)
            val identificacion= "identificacion"
            database.child("users").child(userId).child("username").setValue(identificacion)




            val passwordRegex = Pattern.compile("^" +
                    "(?=.*[-@#$%^&+/=])" +     // Al menos 1 carácter especial
                    ".{6,}" +                // Al menos 6 caracteres
                    "$")


            if(mEmail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
                Toast.makeText(baseContext, "Ingrese un email valido.",
                    Toast.LENGTH_SHORT).show()
            } else if (mPassword.isEmpty() || !passwordRegex.matcher(mPassword).matches()) {
                Toast.makeText(baseContext, "La contraseña no cumple con el criterio: Al menos 6 caracteres y 1 carácter especial y ",
                    Toast.LENGTH_SHORT).show()
            } else if (mPassword != mRepeatPassword) {
                Toast.makeText(baseContext, "Verifique la contraseña",
                    Toast.LENGTH_SHORT).show()
            } else {
                createAccount(mEmail, mPassword)
            }


        }
    }

    private fun createAccount(email : String, password : String) {

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Toast.makeText(baseContext, "Registro Exitoso.",
                        Toast.LENGTH_LONG).show()
                } else {
                    Log.w("TAG", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }




}



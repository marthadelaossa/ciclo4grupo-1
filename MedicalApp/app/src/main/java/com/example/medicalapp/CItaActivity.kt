package com.example.medicalapp

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.medicalapp.databinding.ActivityCitaBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference




@Suppress("DEPRECATION")
class CItaActivity : AppCompatActivity() {
    val database = Firebase.database
    private lateinit var auth: FirebaseAuth
    private lateinit var binding: ActivityCitaBinding
    private val fileResult = 1
    var horas = arrayOf("8:00 a.m", "9:00 a.m", "10:00 a.m", "11:00 a.m","12:00 a.m", "2:00 p.m","3:00 p.m", "4:00 p.m","5:00 p.m")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCitaBinding.inflate(layoutInflater)
        setContentView(binding.root)
        auth = Firebase.auth

        binding.updateProfileAppCompatButton.setOnClickListener {
            Toast.makeText(this, "Su cita ha sido agendada.",
                Toast.LENGTH_SHORT).show()
        }





        binding.signOutImageView.setOnClickListener {
            signOut()
        }

        binding.profileImageView.setOnClickListener {
            fileManager()
        }


        updateUI()

    }

    private fun fileManager() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(intent, fileResult)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == fileResult) {
            if (resultCode == RESULT_OK && data != null) {
                val uri = data.data

                uri?.let { imageUpload(it) }

            }
        }


    }

    private fun imageUpload(mUri: Uri) {

        val user = auth.currentUser
        val folder: StorageReference = FirebaseStorage.getInstance().reference.child("Users")
        val fileName: StorageReference = folder.child("img"+user!!.uid)

        fileName.putFile(mUri).addOnSuccessListener {
            fileName.downloadUrl.addOnSuccessListener { uri ->

                val profileUpdates = userProfileChangeRequest {
                    photoUri = Uri.parse(uri.toString())
                }

                user.updateProfile(profileUpdates)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(this, "Se realizaron los cambios correctamente.",
                                Toast.LENGTH_SHORT).show()
                            updateUI()
                        }
                    }
            }
        }.addOnFailureListener {
            Log.i("TAG", "file upload error")
        }
    }




    private fun signOut() {
        Firebase.auth.signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun updateUI() {
        val user = auth.currentUser

        if(user != null) {
            binding.emailTextView.text = user.email
            binding.nameTextView.text = user.displayName

        }
    }
}